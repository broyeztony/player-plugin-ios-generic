//
//  ShUserData.swift
//  iosGenericPlugin
//
//  Created by Tony on 30/12/2015.
//  Copyright © 2015 Tony. All rights reserved.
//

import Foundation
import UIKit

@objc open class ShUserData: NSObject, NSCoding {
    
    @objc open var sessionId: String
    
    @objc public static let DocumentsDirectory   = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    @objc public static let ArchiveURL           = DocumentsDirectory.appendingPathComponent("streamhub")
    
    struct PropertyKey {
        static let sessionIdKey = "sessionId"
    }
    
    @objc init(sessionId: String) {
        self.sessionId = sessionId
    }
    
    @objc open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.sessionId, forKey: PropertyKey.sessionIdKey)
    }
    
    @objc required convenience public init?(coder aDecoder: NSCoder) {
        
        guard   let S = aDecoder.decodeObject(forKey: PropertyKey.sessionIdKey) as? String
            else { return nil }
        
        // Must call designated initializer.
        self.init(sessionId: S)
    }
    
}
