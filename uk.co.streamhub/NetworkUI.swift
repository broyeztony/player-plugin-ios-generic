//
//  NetworkUI.swift
//  iosGenericPlugin
//
//  Created by Tony on 30/12/2015.
//  Copyright © 2015 Tony. All rights reserved.
//

import Foundation
import AdSupport
import UIKit

@objc open class NetworkUI : NSObject {
    
    static var apiEndpoint          = "https://stats.streamhub.io/api/"
    static var playerAPI            = "player/"
    static var playereventAPI       = "playerevent/"
    static var advertisementAPI     = "advertisement/"
    
    // Events Constants
    static var MEDIA_ERROR          = "player_error";
    static var MEDIA_LOAD_START     = "media_load_start";
    static var MEDIA_LOADED         = "media_loaded";
    static var MEDIA_METADATA       = "media_metadata";
    static var MEDIA_BUFFERING	    = "buffering";
    static var MEDIA_PRE_BUFFERING  = "prebuffering";
    static var MEDIA_START          = "player_start";
    static var MEDIA_COMPLETE       = "player_play_completed";
    static var MEDIA_COMPLETION     = "completion";
    static var AD_CLICK_THROUGH     = "click";
    
    var userAgent:String            = ""
    var _IDFA:String                = ""
    
    @objc override init() {
    
        let ASI = ASIdentifierManager.shared()
        _IDFA   = (ASI.advertisingIdentifier.uuidString)
    }
    
    @objc func getDeviceOs() -> String {
        
        return UIDevice.current.localizedModel + ", " + UIDevice.current.systemName + " " + UIDevice.current.systemVersion
    }
    
    @objc func sendTick(_ startTime:String, publicId:String, partnerId:String, analyticsId:String, playerId:String,
        isLive:Bool, bitrate:String, randomSessionKey:String, sessionId:String, userId:String, channelId:String? ){
            
            let refUrl:String                       = ""
            let locationUrl:String                  = ""
            let agent:String                        = userAgent // To be checked
            let bitrate:String                      = "" // To be checked
            let os                                  = getDeviceOs();
            
            var urlParams:[String: String]          = [String: String]()
            urlParams[ "startTime" ]                = startTime;
            urlParams[ "publicId" ]                 = publicId;
            urlParams[ "partnerId" ]                = partnerId;
            urlParams[ "analyticsId" ]              = analyticsId;
            urlParams[ "playerId" ]                 = playerId;
            urlParams[ "isLive" ]                   = String(isLive);
            urlParams[ "refUrl" ]                   = refUrl;
            urlParams[ "locationUrl" ]              = locationUrl;
            urlParams[ "agent" ]                    = agent;
            urlParams[ "os" ]                       = os;
            urlParams[ "bitrate" ]                  = bitrate;
            urlParams[ "randomSessionKey" ]         = randomSessionKey;
            urlParams[ "sessionId" ]                = sessionId;
            urlParams[ "userId" ]                   = userId;
            urlParams[ "advertisingId" ]            = _IDFA;
        
            if((channelId) != nil){
                urlParams[ "parentPublicId" ]       = channelId!;
            }
        
            let urlParamsAsString = getUrlParamsFromParamsMap(urlParams: urlParams);
            let urlString = NetworkUI.apiEndpoint + NetworkUI.playerAPI + "?" + urlParamsAsString;
            let safeUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let url = URL(string: safeUrlString!)!
            let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                if(error != nil) {
                    print("@@ error", error!)
                }
                print("@@ response", response!)
            })
            task.resume()    
    }
    
    @objc func sendEvent(_ eventName:String, eventParameters: [String: String]?, startTime:String, publicId:String,
       partnerId:String, analyticsId:String, playerId:String, isLive:Bool, bitrate:String, randomSessionKey:String, sessionId:String, userId:String, channelId:String?) {
    
        let refUrl:String                       = ""
        let locationUrl:String                  = ""
        let agent:String                        = userAgent // To be checked
        let bitrate:String                      = "" // To be checked
        let os                                  = getDeviceOs();
        
        var urlParams:[String: String]          = [String: String]()
        urlParams[ "startTime" ]                = startTime;
        urlParams[ "publicId" ]                 = publicId;
        urlParams[ "partnerId" ]                = partnerId;
        urlParams[ "analyticsId" ]              = analyticsId;
        urlParams[ "playerId" ]                 = playerId;
        urlParams[ "isLive" ]                   = String(isLive);
        urlParams[ "refUrl" ]                   = refUrl;
        urlParams[ "locationUrl" ]              = locationUrl;
        urlParams[ "agent" ]                    = agent;
        urlParams[ "os" ]                       = os;
        urlParams[ "bitrate" ]                  = bitrate;
        urlParams[ "randomSessionKey" ]         = randomSessionKey;
        urlParams[ "sessionId" ]                = sessionId;
        urlParams[ "userId" ]                   = userId;
        urlParams[ "advertisingId" ]            = _IDFA;
        
        if((channelId) != nil){
            urlParams[ "parentPublicId" ]       = channelId!;
        }
        
        urlParams[ "event" ]                    = eventName;
        
        if(eventParameters != nil){
            for (eventParamName, eventParamValue) in eventParameters! {
                urlParams[ eventParamName ] = eventParamValue
            }
        }
        
        let urlParamsAsString = getUrlParamsFromParamsMap(urlParams: urlParams);
        let urlString = NetworkUI.apiEndpoint + NetworkUI.playereventAPI + "?" + urlParamsAsString;
        let safeUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: safeUrlString!)!
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if(error != nil) {
                print("@@ error", error!)
            }
            print("@@ response", response!)
        })
        task.resume()
    }
    
    @objc func sendAdEvent(_ eventName:String, adInfo: [String: String], percentile:Int, parentPublicId: String, partnerId: String, analyticsId: String, playerId: String, randomSessionKey: String, sessionId: String, userId: String) {

        if let adId:String = adInfo["id"] {
        
            var urlParams:[String: String]          = [String: String]()
            
            urlParams[ "publicId" ]                 = adId;
            urlParams[ "partnerId" ]                = partnerId;
            urlParams[ "analyticsId" ]              = analyticsId;
            urlParams[ "playerId" ]                 = playerId;
            urlParams[ "userId" ]                   = userId;
            urlParams[ "agent" ]                    = userAgent;
            urlParams[ "randomSessionKey" ]         = randomSessionKey;
            urlParams[ "sessionId" ]                = sessionId;
            urlParams[ "event" ]                    = eventName;
            urlParams[ "parentPublicId" ]           = parentPublicId;
            urlParams[ "percentile" ]               = String(percentile);
            urlParams[ "advertisingId" ]            = _IDFA;
            
            for (eventParamName, eventParamValue) in adInfo {
                if( eventParamName == "campaignId" ||
                    eventParamName == "distributorId" ||
                    eventParamName == "advertiserId" ||
                    eventParamName == "code" ||
                    eventParamName == "customField")
                {
                    urlParams[ eventParamName ] = eventParamValue
                }
            }
            
            let urlParamsAsString = getUrlParamsFromParamsMap(urlParams: urlParams);
            let urlString = NetworkUI.apiEndpoint + NetworkUI.advertisementAPI + "?" + urlParamsAsString;
            let safeUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let url = URL(string: safeUrlString!)!
            let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                if(error != nil) {
                    print("@@ error", error!)
                }
                print("@@ response", response!)
            })
            task.resume()
        }
    }
    
    @objc func getUrlParamsFromParamsMap(urlParams: [String: String]) -> String {
        return (urlParams.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }) as Array).joined(separator: "&")
    }
    
}

