//
//  StreamhubAnalytics.swift
//
//  Created by Tony on 29/12/2015.
//  Copyright © 2015 Tony. All rights reserved.
//

import Foundation
import UIKit


@objc open class StreamhubAnalytics : NSObject {
    
    var _startTime:Float                        = 0.0
    
    var _channelId:String?
    var _publicId:String?
    var _partnerId:String
    var _endPoint:String
    var _playerId:String
    var _isLive:Bool
    var _userId:String
    var _analyticsId:String
    var _sessionId:String
    var _randomSessionKey:String                = UUID().uuidString
    
    var _completionRatesLabels:Array<String>    = [ "1", "25", "50", "75", "95" ];
    var _trackableCompletionRates:Array<Float>  = [ 0.01, 0.25, 0.50, 0.75, 0.95 ];
    var _completionRateStopPoints:Array<Int>    = [ 0, 0, 0, 0, 0 ];
    var _mediaDuration:Float                    = -1;
    var _tickerMap:[Float: Bool]                = [Float: Bool]()
    var _completionMap:[Int: Bool]              = [Int: Bool]()
    var _metadata:[String: String]              = [String: String]()
    var _networkUI:NetworkUI                    = NetworkUI()

    /** Constants for seamless 3rd party integration */
    static var DURATION                         = "duration";
    static var CATEGORIES                       = "categories";
    static var PLAYER_TITLE                     = "playerTitle";
    static var TITLE                            = "title";
    
    
    @objc public init(partnerId:String, endPoint:String, playerId:String, isLive:Bool, userId:String, analyticsId:String) {
        
        _partnerId      = partnerId
        _endPoint       = endPoint
        _playerId       = playerId
        _isLive         = isLive
        _userId         = userId
        _analyticsId    = analyticsId
        
        // Get or Set sessionId
        
        var userData = NSKeyedUnarchiver.unarchiveObject(withFile: ShUserData.ArchiveURL.relativePath) as? ShUserData
        if (userData == nil)
        {
            userData = ShUserData( sessionId: UUID().uuidString )
            NSKeyedArchiver.archiveRootObject( userData!, toFile: ShUserData.ArchiveURL.relativePath)

            _sessionId = (userData?.sessionId)!
        }
        else {
            _sessionId = (userData?.sessionId)!
        }

        print("[StreamhubAnalytics plugin] sessionId: ", _sessionId)
    }
    
    @objc public func reset(){
        _mediaDuration  = -1
        _tickerMap      = [:]
        _completionMap  = [:]
        _metadata       = [:]
    }
    
    @objc public func setChannelId(_ channelId:String){
        _channelId = channelId;
    }
    
    @objc public func setUserAgent(_ userAgent:String){
        _networkUI.userAgent = userAgent;
    }
    
    @objc public func setDuration(_ duration: Float){
        
        print( "[StreamhubAnalytics plugin] setDuration: ", duration );
        print("")
        
        if( self._mediaDuration < 0 && duration > 0 ){
            self._mediaDuration = duration;
            let d = Int(duration);
            addMediaMetadata(key: StreamhubAnalytics.DURATION, value: String(d))
        }
        
        print( "[StreamhubAnalytics plugin] duration: ", duration )
        
        for (index, rate) in self._trackableCompletionRates.enumerated() {
            
            let completionRateStopPoint:Int = Int( _mediaDuration * rate );
            
            if(completionRateStopPoint == 0) {
                
                self._completionRateStopPoints[ index ]     = 1
            }
            else
            {
                self._completionRateStopPoints[ index ]     = completionRateStopPoint
            }
        }
        
        print( "[StreamhubAnalytics plugin] is using completion rates reporting. " +
            "Completion rate events should be sent at rates: ", _completionRateStopPoints )
    }
    
    @objc public func onTick( _ playbackPositionInSec:Float ){
        
        // print( "[StreamhubAnalytics plugin] onTick ", playbackPositionInSec )
        // print("")
        
        let roundedPlaybackPositionInSec    = Int( playbackPositionInSec );
        let startTime                       = Float(roundedPlaybackPositionInSec) / 60.0;
        
        // send ticks every 5 secs for [0mn - 1mn], every minutes after
        self.trackDefaultStrategy( roundedPlaybackPositionInSec, startTime: startTime );
        
        if( _mediaDuration > 0 ){ // Evaluate the need for completion rate reporting
            
            for (index, stopPoint) in self._completionRateStopPoints.enumerated() {
                
                if( roundedPlaybackPositionInSec == stopPoint ){
                    
                    if let _ = _completionMap[ stopPoint ]
                    {
                        return
                    }else
                    {
                        let matchingRateLabel:String    = _completionRatesLabels[ index ];
                        _completionMap[ stopPoint ]     = true;
                        
                        // Send event
                        let completion:[String: String]  = [ "completionRate": matchingRateLabel ]
                        
                        if((_publicId) != nil){
                            _networkUI.sendEvent(NetworkUI.MEDIA_COMPLETION, eventParameters: completion, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
                            
                            print("[StreamhubAnalytics plugin] Sending completion rate for matchingRateLabel: ", matchingRateLabel );
                        }
                    }
                }
            }
        }
    }
    
    @objc func trackDefaultStrategy( _ rpps:Int, startTime:Float){
        
        if( (rpps >= 60 &&
            rpps % 60 == 0) || // >=1mn => tick every mn
            (rpps < 60 &&
            rpps % 5 == 0)) // <1mn => tick every 5sec
        {
            trackTicker(startTime)
        }
        
    }
    
    @objc func trackTicker(_ startTime:Float){
        
        if let _ = _tickerMap[ startTime ]
        {
            return
        }
        
        print("[StreamhubAnalytics plugin] trackTicker: ", startTime );
        
        _tickerMap[ startTime ] = true;
        _startTime              = startTime;
        
        // Send tick
        if((_publicId) != nil){
            _networkUI.sendTick(String(startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
        }
    }
    
    // Events API
    @objc public func onMediaReady(_ publicId:String) {
        
        if(publicId.isEmpty) {
            
            print( "Missing mandatory parameter publicId." );
            return;
        }
        
        _publicId   = publicId;
    }

    /** First event that should be broadcasted */
    @objc public func onMediaLoaded() {
        
        // Send event
        _networkUI.sendEvent(NetworkUI.MEDIA_LOADED, eventParameters: nil, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
    };

    @objc public func onMediaError( _ errorDetails:[String: String] ) {
        if((_publicId) != nil){
            _networkUI.sendEvent(NetworkUI.MEDIA_ERROR, eventParameters: errorDetails, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
        }
    }
    
    @objc func urlEncode( value: String ) -> String? {
        let allowedCharacters = NSCharacterSet.urlFragmentAllowed
        let encodedString = value.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
        return encodedString
    }
    
    /** Metadata Front Loading */
    @objc public func addMediaMetadata(key: String, value: String) -> Bool {
        
        if(key == StreamhubAnalytics.CATEGORIES){
            
            let categories = value.split(separator: ",");
            let valueEncoded = categories.map({ urlEncode(value: String($0))! }).joined(separator: ",")
            _metadata[ key ] = valueEncoded
            
            print("adding metadata | ", key, " -> ", value, " -> ", _metadata[key])
            return true
        } else if(key == StreamhubAnalytics.PLAYER_TITLE || key == StreamhubAnalytics.TITLE || key == StreamhubAnalytics.DURATION) {
            _metadata[ key ] = urlEncode(value: value)
            print("adding metadata | ", key, " -> ", value, " -> ", _metadata[key])
            return true
        }
        
        return false
    };
    
    @objc public func onMediaMetadata() {
        
        let  hasPlayerTitle = _metadata[ "playerTitle" ] != nil
        let hasTitle = _metadata[ "title" ] != nil
        
        if(hasPlayerTitle && hasTitle) {
            // Send event MEDIA_METADATA event
            _networkUI.sendEvent(NetworkUI.MEDIA_METADATA, eventParameters: _metadata, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
        }
    };
    
    @objc public func onMediaBufferedComplete(bufferingTime:Int, prebuffering: Bool) {
        
        var eventName = NetworkUI.MEDIA_BUFFERING;
        if(prebuffering) {
            eventName = NetworkUI.MEDIA_PRE_BUFFERING;
        }

        let bufferingTimeEventParams:[String: String]  =
            [ "bufferingTime": String(bufferingTime) ]

        _networkUI.sendEvent(eventName, eventParameters: bufferingTimeEventParams, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
    }
    
    @objc public func onMediaStart() {
        
        // Send event
        if((_publicId) != nil){
            _networkUI.sendEvent(NetworkUI.MEDIA_START, eventParameters: nil, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
        }
    }
    
    @objc public func onMediaComplete() {
        
        if((_publicId) != nil){
            _networkUI.sendEvent(NetworkUI.MEDIA_COMPLETE, eventParameters: nil, startTime: String(_startTime), publicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, isLive: _isLive, bitrate: "", randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId, channelId: _channelId)
        }
        // Reset state for next video
        reset();
    }
    
    @objc public func trackAd(_ adInfo: [String: String], percentile: Int){
        
        _networkUI.sendAdEvent(NetworkUI.MEDIA_COMPLETION, adInfo: adInfo, percentile: percentile,
            parentPublicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId)
    }
    
    @objc public func onAdClick(_ adInfo: [String: String], percentile: Int){
        
        _networkUI.sendAdEvent(NetworkUI.AD_CLICK_THROUGH, adInfo: adInfo, percentile: percentile,
            parentPublicId: _publicId!, partnerId: _partnerId, analyticsId: _analyticsId, playerId: _playerId, randomSessionKey: _randomSessionKey, sessionId: _sessionId, userId: _userId)
    }

}
