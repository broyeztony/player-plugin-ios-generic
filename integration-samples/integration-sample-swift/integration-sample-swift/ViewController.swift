//
//  ViewController.swift
//  iosGenericPlugin
//
//  Created by Tony on 29/12/2015.
//  Copyright © 2015 Tony. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class ViewController: UIViewController {
    
    var streamhub:StreamhubAnalytics?
    var timeObserver: AnyObject?
    var player: AVPlayer?
    var startTime:NSDate?
    var isFirstTime:Bool = true;
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Creates instance of StreamhubAnalytics
        streamhub     = StreamhubAnalytics(partnerId: "lambda",
                                           endPoint: "http://stats.streamhub.io",
                                           playerId: "ios-swift-generic-test-player",
                                           isLive: false,
                                           userId: "",
                                           analyticsId: "streamhub-5812d")
        streamhub!.setUserAgent("My-Platform-User-Agent")
        streamhub!.onMediaReady("sara-screen-recording")
        streamhub!.onMediaLoaded()
        //
        
        // setup the player
        let videoURL = URL(string: "http://content.jwplatform.com/videos/H4QSqnH6-2TDOeXUh.mp4")
        let avItem = AVPlayerItem(url: videoURL!)
        startTime = NSDate()
        let asset = AVURLAsset(url: videoURL!)
        durationChanged( Float(CMTimeGetSeconds(asset.duration)) )
        
        player = AVPlayer(playerItem: avItem)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player!.play()
        
        // track player playback completed event
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.playerDidFinishPlaying(_:)),
        name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avItem)
        
        // track player tick events
        if (timeObserver == nil) {
            timeObserver = player!.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 100), queue: DispatchQueue.main,
                using: { (time: CMTime) -> Void in

                    let seconds:Float = Float(CMTimeGetSeconds(time))
                    if (!seconds.isNaN && !seconds.isInfinite) {
                        self.positionUpdated(seconds)
                    }
            }) as AnyObject?
        }
        
        // Need to track ads ?
        
        let adInfo:[String: String]  = [ "id": "6128", "advertiserId": "487" ]
        streamhub!.trackAd(adInfo, percentile: 0)
        streamhub!.onAdClick(adInfo, percentile: 0)
        
    }
    
    func positionUpdated(_ seconds: Float) { // playhead updates
        
        // NSLog("playhead position updated: %fd", seconds)
        if(isFirstTime){
            let end = NSDate()
            let timeInterval: Double = end.timeIntervalSince(startTime! as Date)
            print("@@ Prebuffering Time Interval ", timeInterval)
            streamhub!.onMediaBufferedComplete(bufferingTime: Int(timeInterval), prebuffering: true)
            isFirstTime = false;
            streamhub!.onMediaStart()
        }
        
        streamhub!.onTick(seconds)
    }
    
    func durationChanged(_ seconds: Float){
    
        streamhub!.setDuration(seconds) // inform about the media duration, so that completion rates event can be emitted
        
        // Provide media metadata to Streamhub. Allowed metadata keys are title, playerTitle and categories
        streamhub!.addMediaMetadata( key: StreamhubAnalytics.TITLE, value: "SARA Screen Recording" )
        streamhub!.addMediaMetadata( key: StreamhubAnalytics.PLAYER_TITLE, value: "My awesome player")
        streamhub!.addMediaMetadata( key: StreamhubAnalytics.CATEGORIES, value: "some,meaningful,categories")
        
        streamhub!.onMediaMetadata();
    }
    
    @objc func playerDidFinishPlaying(_ note: Notification) {
        streamhub!.onMediaComplete()
    }
}

