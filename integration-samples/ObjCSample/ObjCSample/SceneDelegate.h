//
//  SceneDelegate.h
//  ObjCSample
//
//  Created by tony on 20/08/2020.
//  Copyright © 2020 tony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

