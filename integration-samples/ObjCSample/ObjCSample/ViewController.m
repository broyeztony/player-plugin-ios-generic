//
//  ViewController.m
//  ObjCSample
//
//  Created by tony on 20/08/2020.
//  Copyright © 2020 tony. All rights reserved.
//

#import "ViewController.h"
#import "ObjCSample-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialise the StreamhubAnalytics plugin
    StreamhubAnalytics* streamhub = [[StreamhubAnalytics alloc] initWithPartnerId:@"lambda" endPoint:@"http://stats.streamhub.io" playerId:@"ios-swift-generic-test-player" isLive:NO userId:@"" analyticsId:@"streamhub-5812d"];
    
    NSLog(@"streamhub: %@", streamhub);
    
    // Set the user-agent
    [streamhub setUserAgent:@"My-Platform-User-Agent"];
    
    // Set the program public id. Logs are resolved against this identifier.
    // It should designate an asset uniquely and with no ambiguity
    [streamhub onMediaReady:@"sara-screen-recording"];
    
    // Set the program duration. Required to emit the completion rate events
    [streamhub setDuration:68.9f];
    
    // Initialise the beginning of the playback tracking
    [streamhub onMediaStart];
    
    // Track heartbeats. Simulating playhead position progress here
    float timeInterval = 5.0f; // 5 seconds
    float currentPosition = 0.0f;
    while (currentPosition < 68.9f) {
        
        NSLog(@"currentPosition: %f", currentPosition);
        [streamhub onTick: currentPosition];
        currentPosition += timeInterval;
        [NSThread sleepForTimeInterval: 5.0f]; // sleep for 5 seconds
    }
    
    // Notify the playback is complete
    [streamhub onMediaComplete];
    
    NSLog(@"@@ playback tracking complete.");
}


@end
